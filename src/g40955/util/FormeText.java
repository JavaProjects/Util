package g40955.util;

/**
* Cette classe permet de changer la forme d'un texte
* @author Matthieu Schmit - 17 nov 2014
*/

public class FormeText {

	/**
	* Methode de mise à default du BASH.
	* @return La forme par default
	*/
	private static String toDefault() {
		return "\033[0m";
	}

	/**
	* Met en gras une chaine.
	* @param a La chaine à mettre en gras
	* @return La chaine en gras
	*/
	public static String toBold (String a) {
		return "\033[1m"+a+toDefault();
	}

	/**
	* Met en plus fin une chaine.
	* @param a La chaine à mettre en plus fin
	* @return La chaine en plus fin
	*/
	public static String toDim (String a) {
		return "\033[2m"+a+toDefault();
	}

	/**
	* Met un texte en italique.
	* @param a La chaine à mettre en italique
	* @return La chaine en italique
	*/
	public static String toItalic (String a) {
		return "\033[3m"+a+toDefault();
	}

	/**
	* Souligne une chaine.
	* @param a La chaine à souligner
	* @return La chaine soulignée
	*/
	public static String toUnderlined (String a) {
		return "\033[4m"+a+toDefault();
	}

	/**
	* Fait clignoter une chaine.
	* @param a La chaine à faire clignoter
	* @return La chaine qui clignote
	*/
	public static String toBlink (String a) {
		return "\033[5m"+a+toDefault();
	}

	/**
	* Inverse la couleur d'une chaine et la couleur du background.
	* @param a La chaine avec couleurs à inverser
	* @return La chaine avec couleurs inversées
	*/
	public static String toReverse (String a) {
		return "\033[7m"+a+toDefault();
	}

	/**
	* Cache une chaine.
	* @param a La chaine à cacher
	* @return La chaine cachée
	*/
	public static String toHidden (String a) {
		return "\033[8m"+a+toDefault();
	}

	/**
	* Barre une chaine.
	* @param a La chaine à barrer
	* @return La chaine barrée
	*/
	public static String toCross (String a) {
		return "\033[9m"+a+toDefault();
	}

	/**
	* Passages à la lignes.
	* @param n Nombre de passages à la ligne à faire
	*/
	public static void Back (int n) {
		for (int i=1 ; i<=n ; i++) {
			System.out.println("\n");
		}
	}

	public static void main (String [] args) {}

}
