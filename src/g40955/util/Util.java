package g40955.util;

import java.util.Scanner;
import java.util.Random;

/**
* @author Matthieu Schmit - 23 nov 2014
*/

public class Util {
	public static void main (String [] args) {
	    System.out.println(LireEntier("Entre un entier ", "Mauvaise entrée.."));
	    System.out.println(LireEntierPositif("Entre un entier positif ", "Pas un entier", "Pas positif"));
	    System.out.println(LireEntierBorne("Entre un entier positif entre -5 et 2 ", -5, 2, "Pas un entier"
	    					, "pas compris entre -5 et 2"));
	}

	/**
	* Demande à l'utilsateur d'entrer un entier.
	* @param msg Le message indiqué
	* @param err Le message d'erreur si l'utilisateur n'entre pas un entier
	* @return L'entier lu
	*/
	public static int LireEntier (String msg, String err) {
		Scanner clavier = new Scanner (System.in);
		System.out.print(msg);
		while (! clavier .hasNextInt()) {
			System.out.println(err);
			clavier.next();
		}
		return clavier.nextInt();
	}

	/**
	* Demande à l'utilisateur d'entrer un entier positif.
	* @param msg Le message indiqué
	* @param err1 Le message d'erreur si l'utilisateur n'entre pas un entier
	* @param err2 Le message d'erreur si le nombre n'est pas positif
	* @return L'entier positif lu
	*/
	public static int LireEntierPositif (String msg, String err1, String err2) {
	    int nb;
	    nb = LireEntier(msg, err1);
	    while (nb < 0) {
		System.out.println(err2);
		nb = LireEntier(msg, err1);
	    }
	    return nb;
	}

	/**
	* Demande à l'utilisateur d'entrer un entier borné (bornes comprises).
	* @param msg Le message indiqué
	* @param min La borne minimale
	* @param max La borne maximale
	* @param err1 Le message d'erruer si l'utilisateur n'entre pas un entier
	* @param err2 Le message d'erreur si l'entier n'est pas entre les bornes
	* @return L'entier, borné, lu
	*/
	public static int LireEntierBorne (String msg, int min, int max, String err1, String err2) {
	    int nb;
	    nb = LireEntier(msg, err1);
	    while (nb < min || nb > max) {
		System.out.println(err2);
		nb = LireEntier(msg, err1);
	    }
	    return nb;
	}

	/**
	* Renvoie un entier aléatoire entre deux bornes.
	* @param min La borne minimale
	* @param max La borne maximale
	* @return L'entier aléatoire
	*/
	public static int Hasard (int min, int max) {
	    Random rand = new Random();
	    return rand.nextInt(min - max + 1) + min;
	}

	/**
	* Demande à l'utilisateur d'entrer un réel.
	* @param msg Le message indiqué
	* @param err Le message d'erreur
	* @return Le réel lu
	*/
	public static double LireReel (String msg, String err) {
	    Scanner clavier = new Scanner (System.in);
	    System.out.print(msg);
	    while (!clavier .hasNextDouble()) {
		System.out.println(err);
		clavier.next();
	    }
	    return clavier.nextDouble();
	}

	/**
	* Demande à l'utilisateur d'entrer un réel positif.
	* @param msg Le message indiqué
	* @param err1 Le message d'erreur si l'utilisateur n'entre pas un reel
	* @param err2 Le message d'erreur si l'utilisateur n'entre pas un réel positif
	* @return Le réel positif
	*/
	public static double LireReelPositif (String msg, String err1, String err2) {
		double nb;
		nb = LireReel(msg, err1);
		while (nb < 0) {
			System.out.println(err2);
			nb = LireReel(msg, err1);
		}
		return nb;
	}
	
	/**
	* Demande à l'utilisateur d'entrer un réel borné, bornes comprises.
	* @param msg Le message indiqué
	* @param min La borne minimale
	* @param max La borne maximale
	* @param err1 Le message d'erreur si l'utilisateur n'entre pas un réel
	* @param err2 Le message d'erreur si l'utilisateur entre un réel hors des bornes
	* @return Le réel borné
	*/
	public static double LireReelBorne (String msg, double min, double max, String err1, String err2) {
		double nb;
		nb = LireReel(msg, err1);
		while (nb < min || nb > max) {
			System.out.println(err2);
			nb = LireReel(msg, err2);
		}
		return nb;
	}
}
