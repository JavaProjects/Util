package g40955.util;

/**
* Colorie le fond d'un texte
* @author Matthieu Schmit - 17 nov 2014
*/

public class Background {

	/**
	* Methode de fond par default du BASH.
	* @return le fond par default
	*/
	public static String toDefault () {
		return "\033[49m";
	}

	/** Colorie le fond en rose.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond coloré
	*/
	public static String toPink (String a) {
		return "\033[48;5;201m"+a+toDefault();
	}

	/**
	* Colorie le fond en noir.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toBlack (String a) {
		return "\033[40m"+a+toDefault();
	}

	/**
	* Colorie le fond en rouge.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toRed (String a) {
		return "\033[41m"+a+toDefault();
	}

	/**
	* Colorie le fond en vert.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toGreen (String a) {
		return "\033[42m"+a+toDefault();
	}

	/**
	* Colorie le fond en jaune.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toYellow (String a) {
		return "\033[43m"+a+toDefault();
	}

	/**
	* Colorie le fond en bleu.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toBlue (String a) {
		return "\033[44m"+a+toDefault();
	}

	/**
	* Colorie le fond en magenta.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toMagenta (String a) {
		return "\033[45m"+a+toDefault();
	}

	/**
	* Colorie le fond en cyan.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toCyan (String a) {
		return "\033[46m"+a+toDefault();
	}

	/**
	* Colorie le fond en gris.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toGray (String a) {
		return "\033[47m"+a+toDefault();
	}

	/**
	* Colorie le fond en gris foncé.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toGrayDark (String a) {
		return "\033[100m"+a+toDefault();
	}

	/**
	* Colorie le fond en rouge fluo.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toRedFluo (String a) {
		return "\033[101m"+a+toDefault();
	}

	/**
	* Colorie le fond en vert fluo.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toGreenFluo (String a) {
		return "\033[102m"+a+toDefault();
	}

	/**
	* Colorie le fond en jaune fluo.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toYellowFluo (String a) {
		return "\033[103m"+a+toDefault();
	}

	/**
	* Colorie le fond en bleu fluo.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toBlueFluo (String a) {
		return "\033[104m"+a+toDefault();
	}

	/**
	* Colorie le fond en magenta fluo.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toMagentaFluo (String a) {
		return "\033[105m"+a+toDefault();
	}

	/**
	* Colorie le fond en cyan fluo.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toCyanFluo (String a) {
		return "\033[106m"+a+toDefault();
	}

	/**
	* Colorie le fond en blanc.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toWhite (String a) {
		return "\033[107m"+a+toDefault();
	}

	/**
	* Colorie le fond en orange.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toOrange (String a) {
		return "\033[48;5;202m"+a+toDefault();
	}

	/**
	* Colorie le fond en brun.
	* @param a La chaine avec le fond à colorier
	* @return La chaine avec le fond colorié
	*/
	public static String toBrown (String a) {
		return "\033[48;5;88m"+a+toDefault();
	}

	public static void main (String [] args) {
		System.out.print(toMagentaFluo("Hello "));
		System.out.print(toRed("World "));
		System.out.println(toRedFluo("!"));
	}
}
