package g40955.util;

/**
* Cette classe permet l'écriture d'un texte coloré
* @author Damien Jeanty - 7/10/2014
* 4/11/2104 - Ajout d'un main - MCD
* 17/11/2014 - Ajout de couleurs - Matthieu Schmit
*/
public class Color {
    /**
     * Méthode de couleur default du BASH.
     * @return La couleur default.
     */
    private static String toDefault() {
        return "\033[0m";
    }

    /** Colorie une chaine en rose.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toPink (String a) {
	 return "\033[38;5;201m"+a+toDefault();
    }

    /**
    * Colorie une chaine en brun.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toBrown(String a) {
        return "\033[38;5;88m"+a+toDefault();
    }

    /**
    * Colorie une chaine en orange.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toOrange(String a) {
        return "\033[38;5;202m"+a+toDefault();
    }

    /**
    * Colorie une chaine en rouge fluo.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toRedFluo(String a) {
        return "\033[91m"+a+toDefault();
    }

    /**
    * Colorie une chaine en vert fluo.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toGreenFluo(String a) {
        return "\033[92m"+a+toDefault();
    }

    /**
    * Colorie une chaine en jaune fluo.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toYellowFluo(String a) {
        return "\033[93m"+a+toDefault();
    }

    /**
    * Colorie une chaine en bleu fluo.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toBlueFluo(String a) {
        return "\033[94m"+a+toDefault();
    }

    /**
    * Colorie une chaine en magenta fluo.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toMagentaFluo(String a) {
        return "\033[95m"+a+toDefault();
    }

    /**
    * Colorie une chaine en cyan fluo.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toCyanFluo(String a) {
        return "\033[96m"+a+toDefault();
    }


    /**
    * Colorie une chaine en gris.
    * @param a La chaine à colorer
    * @return La chaine colorée
    */
    public static String toGray(String a) {
        return "\033[90m"+a+toDefault();
    }

    /**
     * Colorie une chaine en noir.
     * @param a La chaine à colorer.
     * @return La chaine colorée.
     */
    public static String toBlack(String a) {
        return "\033[38;5;232m"+a+toDefault();
    }

    /**
     * Colorie une chaine en rouge.
     * @param a La chaine à colorer.
     * @return La chaine colorée.
     */
    public static String toRed(String a) {
        return "\033[31m"+a+toDefault();
    }

    /**
     * Colorie une chaine en vert.
     * @param a La chaine à colorer.
     * @return La chaine colorée.
     */
    public static String toGreen(String a) {
        return "\033[32m"+a+toDefault();
    }

    /**
     * Colorie une chaine en jaune.
     * @param a La chaine à colorer.
     * @return La chaine colorée.
     */
    public static String toYellow(String a) {
        return "\033[33m"+a+toDefault();
    }

    /**
     * Colorie une chaine en bleu.
     * @param a La chaine à colorer.
     * @return La chaine colorée.
     */
    public static String toBlue(String a) {
        return "\033[34m"+a+toDefault();
    }

    /**
     * Colorie une chaine en mauve.
     * @param a La chaine à colorer.
     * @return La chaine colorée.
     */
    public static String toPurple(String a) {
        return "\033[35m"+a+toDefault();
    }

    /**
     * Colorie une chaine en cyan.
     * @param a La chaine à colorer.
     * @return La chaine colorée.
     */
    public static String toCyan(String a) {
        return "\033[36m"+a+toDefault();
    }

    /**
     * Colorie une chaine en blanc.
     * @param a La chaine à colorer.
     * @return La chaine colorée.
     */
    public static String toWhite(String a) {
        return "\033[37m"+a+toDefault();
    }

    /**
     * Affiche un  message coloré pour montrer
     * le but de la classe.
     */
    public static void main(String[] args) {
	System.out.println(
	    toGreen("Bonjour")
	  + toCyan(" le ")
	  + toRed("monde")
	  + toYellow(" !") );
    }
}
