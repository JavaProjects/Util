package g40955.util;

import java.util.Scanner;

/**
* Class qui vérifie si l'utilisateur rentre bien ce qu'on lui demande.
* @author Matthieu Schmit - 17 nov 2014
*/

public class EntreeClavier {


	/**
	* Verifie si l'utilisateur rentre bien un ENTIER compris entre 2 bornes, incluses.
	* @param min La borne minimum
	* @param max La borne maximum
	* @param msg Le message affiché demandant à l'utilisateur de rentrer un nombre
	* @param err Le message d'erreur si l'utilisateur se trompe dans l'entrée
	* @return L'entier rentré par l'utilisateur
	*/
	public static int Entier (int min, int max, String msg, String err) {
		Scanner clavier = new Scanner (System.in);
		int nbClavier = 0;
		boolean okClavier = false;
		System.out.println(msg);
		while (okClavier == false) {
			while (!clavier .hasNextInt() ) {
				clavier.next();
				System.out.println(err);
			}
			nbClavier = clavier.nextInt();
			okClavier = true;
			if (nbClavier < min || nbClavier > max) {
				okClavier = false;
				System.out.println(err);
			}
		}
		return nbClavier;
	}


	/**
	* Verifie si l'utilisateur rentre bien un REEL compris entre 2 bornes, incluses.
	* @param min La borne minimum
	* @param max La borne maximum
	* @param msg Le message affiché demandant à l'utilisateur de rentrer un nombre
	* @param err Le message d'erreur si l'utilisateur se trompe dans l'entrée
	* @return Le reel rentré par l'utilisateur
	*/

	public static double Reel (double min, double max, String msg, String err) {
		Scanner clavier = new Scanner (System.in);
		double nbClavier = 0;
		boolean okClavier = false;
		System.out.println(msg);
		while (okClavier == false) {
			while (!clavier .hasNextDouble() ) {
				clavier.next();
				System.out.println(err);
			}
			nbClavier = clavier.nextDouble();
			okClavier = true;
			if (nbClavier < min || nbClavier > max) {
				okClavier = false;
				System.out.println(err);
			}
		}
		return nbClavier;
	}

	public static void main (String [] args) {
	}
}