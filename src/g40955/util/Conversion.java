/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g40955.util;

/**
 *
 * @author Matthieu
 */
public class Conversion {
    
    public static void main(String[] args) {
        System.out.println(Dec_Hex(42));
        System.out.println(Dec_Oct(42));
    }
    
    public static int Dec_Bin (int nb) {
        int nbB, compt;
        nbB = 0;
        compt = 0;
        while (nb > 0) {
            nbB = nbB + ((nb % 2) * (int) Math.pow(10, compt));
            nb = nb / 2;
            compt ++;
        }
        return nbB;
    }
    
    public static String Dec_Hex (int nb) {
        String nbH;
        int reste;
        boolean okPlein;
        okPlein = false;
        nbH = "";
        while (nb > 0) {
            okPlein = true;
            reste = nb % 16;
            nb = (nb - reste) / 16;
            switch (reste) {
                case 1 :
                case 2 :
                case 3 :
                case 4 :
                case 5 :
                case 6 :
                case 7 :
                case 8 :
                case 9 : 
                    nbH = reste + nbH;
                    break;
                case 10 : nbH = "A" + nbH; break;
                case 11 : nbH = "B" + nbH; break;
                case 12 : nbH = "C" + nbH; break;
                case 13 : nbH = "D" + nbH; break;
                case 14 : nbH = "E" + nbH; break;
                case 15 : nbH = "F" + nbH; break;
                case 0 : nbH = nbH + "0"; break;
            }
        }
        if (!okPlein) {
            nbH = "0";
        }
        return nbH;
    }
    
    public static int Dec_Oct (int nb) {
        String nbO;
        int reste;
        nbO = "";
        while (nb >= 8) {
            reste = nb % 8;
            nb = (nb - reste ) / 8;
            nbO = reste + nbO;
        }
        nbO = nb + nbO;
        return Integer.parseInt(nbO);
    }
}