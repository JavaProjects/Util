package g40955.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class utilisant la date et l'heure du système.
 * @author Matthieu Schmit - 01 mars 2015
 */
public class Time {

    public static void main(String[] args) {
        System.out.println(ThisTime());
        System.out.println(ThisDate());
        System.out.println("Debut interruption");
        Interruption(1);
        System.out.println("Fin interruption");
        System.out.println(ThisHour());
        System.out.println(Ecart("12/09/1993 à 11:50:38", ThisTime()));
        System.out.println(Ecart("16/03/2015 à 15:39:00", ThisTime()));
        System.out.println(ConversionSecJour(Ecart("12/09/1993 à 11:50:38", ThisTime())));
        System.out.println(ThisHour());
        System.out.println(ThisHourH(ThisHour()));
        System.out.println(ThisHourM(ThisHour()));
        System.out.println(ThisHourS(ThisHour()));
    }

    /**
     * Renvoie une chaine contenant la date et l'heure.
     * Sous la forme "dd/MM/yyyy à HH:mm:ss"
     * @return la date et l'heure
     */
    public static String ThisTime() {
        Date actuelle = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy à HH:mm:ss");
        return dateFormat.format(actuelle);
    }

    /**
     * Renvoie une chaine contenant la date.
     * Sous la forme "dd/MM/yyy"
     * @return la date
     */
    public static String ThisDate() {
        Date actuelle = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(actuelle);
    }

    /**
     * Renvoie une chaine contenant l'heure.
     * Sous la forme "HH:mm:ss"
     * @return l'heure
     */
    public static String ThisHour() {
        Date actuelle = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(actuelle);
    }

    /**
     * Met le programme sur pause.
     * @param tmps le temps de la pause, en seconde
     */
    public static void Interruption(double tmps) {
        tmps = tmps * 1000;
        try {
            Thread.sleep((int)tmps);
        } catch (InterruptedException e) {
        }
    }

    /**
     * Calcule le nombre de secondes entre deux moments.
     * D1 est antérieur à D2
     * D1 et D2 sont sous la forme "dd/MM/yyyy à HH:mm:ss" (récupérable par la
     * méthode ThisTime()
     * @param D1 moment 1
     * @param D2 moment 2
     * @return le nombre de seconde
     */
    public static long Ecart(String D1, String D2) {
        long ecart;
        ecart = SecondesDepuis1900(D2) - SecondesDepuis1900(D1);
        return ecart;
    }

    /**
     * Retourne le nombre de secondes entre le 1er janvier 1900 et un moment.
     * D1 est sous la forme "dd/MM/yyyy à HH:mm:ss" (récupérable par la
     * méthode ThisTime()
     * @param D1 le moment
     * @return le nombre de seconde
     */
    private static long SecondesDepuis1900(String D1) {
        int h1, m1, s1, j1, ms1, a1;
        long temps;
        temps = 0;
        h1 = Integer.parseInt(D1.substring(13, 15));
        m1 = Integer.parseInt(D1.substring(16, 18));
        s1 = Integer.parseInt(D1.substring(19, 21));
        j1 = Integer.parseInt(D1.substring(0, 2));
        ms1 = Integer.parseInt(D1.substring(3, 5));
        a1 = Integer.parseInt(D1.substring(6, 10));
        for (int i = 1900; i < a1; i++) {
            if (estBisextile(a1)) {
                temps = temps + 31622400;
            } else {
                temps = temps + 31536000;
            }
        }
        temps = temps+s1+60*m1+3600*h1+86400*(j1-1)+86400*nbJours(D1);
        return temps;
    }

    /**
     * Retourne le nombre de jours entre le 1er janvier et la fin du mois précédent.
     * Ex. Si D1 (23/03/2015...), retourne 59 (31+28)
     * D1 est sous la forme "dd/MM/yyyy à HH:mm:ss" (récupérable par la
     * méthode ThisTime()
     * @param D1 le moment
     * @return le nombre de jour
     */
    private static int nbJours(String D1) {
        int ms1, a1;
        int tmps;
        ms1 = Integer.parseInt(D1.substring(3, 5));
        a1 = Integer.parseInt(D1.substring(6, 10));
        tmps = 0;
        for (int i = 1; i < ms1; i++) {
            switch (i) {
                case 1: tmps = tmps + 31; break;
                case 2:
                    if (estBisextile(a1)) {
                        tmps = tmps + 29;
                    } else {
                    tmps = tmps + 28;
                    }
                    break;
                case 3: tmps = tmps + 31; break;
                case 4: tmps = tmps + 30; break;
                case 5: tmps = tmps + 31; break;
                case 6: tmps = tmps + 30; break;
                case 7: tmps = tmps + 31; break;
                case 8: tmps = tmps + 31; break;
                case 9: tmps = tmps + 30; break;
                case 10: tmps = tmps + 31; break;
                case 11: tmps = tmps + 30; break;
                case 12: tmps = tmps + 31; break;
            }
        }
        return tmps;
    }

    /**
     * Verifie si une année est bissextile.
     * @param a l'année
     * @return vrai si l'année est bissextile
     */
    public static boolean estBisextile(int a) {
        return ((a%4 == 0 && a%100!=0) || a%400 == 0) ;
    }
    
    /**
     * Convertit les secondes en jours
     * @param sec le nombre de secondes
     * @return le nombre de jours
     */
    public static double ConversionSecJour (long sec) {
        return ((double)sec)/86400 ;
    }
    
    /**
     * Recupère l'heure d'un moment.
     * Le moment sous la forme "hh:mm:ss" (récupérable par la
     * méthode ThisHour() )
     * @param h le moment
     * @return l'heure
     */
    public static int ThisHourH (String h) {
        int heure;
        heure = Integer.parseInt(h.substring(0, 2));
        return heure;
    }
    
     /**
     * Recupère les minutes d'un moment.
     * Le moment sous la forme "hh:mm:ss" (récupérable par la
     * méthode ThisHour() )
     * @param h le moment
     * @return les minutes
     */
    public static int ThisHourM (String h) {
        int minute;
        minute = Integer.parseInt(h.substring(3, 5));
        return minute;
    }
    
     /**
     * Recupère les secondes d'un moment.
     * Le moment sous la forme "hh:mm:ss" (récupérable par la
     * méthode ThisHour() )
     * @param h le moment
     * @return les secondes
     */
    public static int ThisHourS (String h) {
        int seconde;
        seconde = Integer.parseInt(h.substring(6, 8));
        return seconde;
    }
}
